<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sail-force-swimming' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'EfDh48UoEjPCfPQj1W7C3k5QrFceePoiJfxVmbQnMbmf1DXWMqJ8j3DmqyOQopFO' );
define( 'SECURE_AUTH_KEY',  'YznmSaml2BWivDmQuqneQMubxVagXhJX3CqfFwRrT7SK3ouAlKsK6oDWkIkbp5Sj' );
define( 'LOGGED_IN_KEY',    'Hq3VLRENI5pbeubbrz1ljyuIhm7l2AHnHI9LcTetTamGdUdf1BKCgBdMYgRZ7BKo' );
define( 'NONCE_KEY',        'mSxLX2o5FAQpXHgASwn2JCosd7FKJgo2yqgLQzmS33BsP6ocShBt4WMfzba2FdcN' );
define( 'AUTH_SALT',        'eAuRc236iGrJqrvo4jVsuQANYsHF8Sox61iSCKGf8XPX19FajIgr4d0R4i1h5L1u' );
define( 'SECURE_AUTH_SALT', 'Su4sEtNCxcjTvE7P6gAidXE0yQhyk5IYIaUmCmEMboorhQNiacm6FXGZNov2Ff6N' );
define( 'LOGGED_IN_SALT',   'lYvStLsDaLnyByMuOLDKKM2LuM0tkxog6YPgUpCrj7wNoqIxpMk6Z89kWI7i7xdr' );
define( 'NONCE_SALT',       'f5JiR7WBywXxuwJSRZrnkQDmKDXwFPqIfzApJAjOtPV0kIUsMfnK6ldCv7nDaWLS' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
